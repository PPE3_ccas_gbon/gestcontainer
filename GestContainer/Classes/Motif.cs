﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Data;

namespace GestContainer
{
    /// <summary>
    /// Représente un motif d'une inspection
    /// </summary>
    /// <remarks>
    /// Les motifs sont prédéfinis
    /// </remarks>
    class Motif
    {
        // Champs de la classe Motif :

        public string CodeMotif { get; set; }
        public string LibelleMotif { get; set; }

        // Champs contenant les requêtes :

        private static string _selectSql = "SELECT * FROM MOTIF";
        private static string _selectByIdSql = "SELECT * FROM MOTIF WHERE codeMotif = ?codeMotif ";
        private static string _updateSql = "UPDATE MOTIF SET codeMotif = ?codeMotif, libelleMotif=?libelleMotif WHERE codeMotif=?codeMotif ";
        private static string _insertSql = "INSERT INTO MOTIF (codeMotif, libelleMotif) VALUES (?codeMotif,?libelleMotif)";
        private static string _deleteByIdSql = "DELETE FROM MOTIF WHERE codeMotif = ?codeMotif";
        private static string _getLastInsertId = "SELECT codeMotif FROM MOTIF WHERE libelleMotif = ?libelleMotif";

        // Constructeur de la classe Motif :

        /// <summary>
        /// initialise un objet de type Inspection
        /// </summary>
        public Motif()
        {
            CodeMotif = "-1";
        }

        // Méthodes de la classe Motif :

        public override string ToString()
        {
            return String.Format("LibelleMotif : {0} ", LibelleMotif);
        }

        #region Méthodes d'accès aux données

        /// <summary>
        /// Valorise un objet motif depuis le système de gestion de bases de données
        /// </summary>
        /// <param name="codeMotif">La valeur de la clé primaire</param>
        public static Motif Fetch(int codeMotif)
        {
            Motif m = null;
            DataBaseAccess.Connexion.Open();//connection à la bd

            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _selectByIdSql;//Définit la requete à utiliser
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeMotif", codeMotif));//Transmet un paramètre à utiliser lors de l'envoie de la requête
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                m = new Motif();//Initialisation de la variable Contact
                m.CodeMotif = jeuEnregistrements["codeMotif"].ToString();//Lecture d'un champ de l'enregistrement
                m.LibelleMotif = jeuEnregistrements["libelleMotif"].ToString();
            }
            DataBaseAccess.Connexion.Close();//fermeture de la connexion
            return m;
        }

        /// <summary>
        /// Sauvegarde ou met à jour un motif dans la base de données
        /// </summary>
        public void Save()
        {
            if (CodeMotif == "-1")
            {
                Insert();
            }
            else
            {
                Update();
            }
        }

        /// <summary>
        /// Supprime le motif représenté par l'instance courante dans le SGBD
        /// </summary>
        public void Delete()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeMotif", CodeMotif));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            CodeMotif = "-1";
        }

        /// <summary>
        /// Met à jour le motif représenté par l'instance courante dans le SGBD
        /// </summary>
        private void Update()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeMotif", CodeMotif));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?libelleMotif", LibelleMotif));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Insère le motif de l'instance courante dans le SGBD
        /// </summary>
        private void Insert()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?libelleMotif", LibelleMotif));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            IDbCommand commandGetLastId = DataBaseAccess.Connexion.CreateCommand();
            commandGetLastId.CommandText = _getLastInsertId;
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?libelleMotif", LibelleMotif));
            commandGetLastId.Prepare();
            IDataReader jeuEnregistrements = commandGetLastId.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                CodeMotif = jeuEnregistrements["codeMotif"].ToString();
            }
            else
            {
                commandSql.Transaction.Rollback();
                throw new Exception("Duplicate entry");
            }
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Retourne une collection contenant les motifs
        /// </summary>
        /// <returns>Une collection de motifs</returns>
        public static List<Motif> FetchAll()
        {
            List<Motif> resultat = new List<Motif>();
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _selectSql;
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Motif motif = new Motif();
                string codeMotif = jeuEnregistrements["codeMotif"].ToString();
                motif.CodeMotif = codeMotif;
                motif.LibelleMotif = jeuEnregistrements["LibelleMotif"].ToString();
                resultat.Add(motif);
            }
            DataBaseAccess.Connexion.Close();
            return resultat;
        }

        #endregion
    }
}
