﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Data;

namespace GestContainer.Classes
{
    /// <summary>
    /// Représente un problème déclaré concernant un container
    /// </summary>
    class Probleme
    {
        // Champs de la classe Probleme :

        public string NumProbleme { get; private set; }
        public string LibelleProbleme { get; set; }

        // Champs contenant les requêtes :

        private static string _selectSql = "SELECT * FROM PROBLEME";
        private static string _selectByIdSql = "SELECT * FROM PROBLEME WHERE numProbleme = ?numProbleme ";
        private static string _updateSql = "UPDATE PROBLEME SET libelleProbleme = ?libelleProbleme WHERE numProbleme=?numProbleme ";
        private static string _insertSql = "INSERT INTO PROBLEME (libelleProbleme) VALUES (?libelleProbleme)";
        private static string _deleteByIdSql = "DELETE FROM PROBLEME WHERE numProbleme = ?numProbleme";
        private static string _getLastInsertId = "SELECT numProbleme FROM PROBLEME WHERE libelleProbleme = ?libelleProbleme";

        // Constructeur de la classe Probleme :

        /// <summary>
        /// Initialise un objet de type Probleme
        /// </summary>
        public Probleme()
        {
            NumProbleme = "-1";//Identifie un probleme non référencé dans la base de données
        }

        // Méthodes de la classe Probleme :

        public override string ToString()
        {
            return String.Format("LibelleProbleme : {0}", LibelleProbleme);
        }

        #region Méthodes d'accès aux données

        /// <summary>
        /// Valorise un objet probleme depuis le système de gestion de bases de données
        /// </summary>
        /// <param name="numProbleme">La valeur de la clé primaire</param>
        public static Probleme Fetch(int numProbleme)
        {
            Probleme p = null;
            DataBaseAccess.Connexion.Open();//connection à la bd

            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _selectByIdSql;//Définit la requete à utiliser
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numProbleme", numProbleme));//Transmet un paramètre à utiliser lors de l'envoie de la requête
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                p = new Probleme();//Initialisation de la variable probleme
                p.NumProbleme = jeuEnregistrements["numProbleme"].ToString();//Lecture d'un champ de l'enregistrement
                p.LibelleProbleme = jeuEnregistrements["libelleProbleme"].ToString();
            }
            DataBaseAccess.Connexion.Close();//fermeture de la connexion
            return p;
        }

        /// <summary>
        /// Sauvegarde ou met à jour un probleme dans la base de données
        /// </summary>
        public void Save()
        {
            if (NumProbleme == "-1")
            {
                Insert();
            }
            else
            {
                Update();
            }
        }

        /// <summary>
        /// Supprime le probleme représenté par l'instance courante dans le SGBD
        /// </summary>
        public void Delete()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numProbleme", NumProbleme));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            NumProbleme = "-1";
        }

        /// <summary>
        /// Met à jour le problème représenté par l'instance courante dans le SGBD
        /// </summary>
        private void Update()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numProbleme", NumProbleme));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?libelleProbleme", LibelleProbleme));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Insère le problème représenté par l'instance courante dans le SGBD
        /// </summary>
        private void Insert()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?libelleProbleme", LibelleProbleme));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            IDbCommand commandGetLastId = DataBaseAccess.Connexion.CreateCommand();
            commandGetLastId.CommandText = _getLastInsertId;
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?libelleProbleme", LibelleProbleme));
            commandGetLastId.Prepare();
            IDataReader jeuEnregistrements = commandGetLastId.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                NumProbleme = jeuEnregistrements["numProbleme"].ToString();
            }
            else
            {
                commandSql.Transaction.Rollback();
                throw new Exception("Duplicate entry");
            }
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Retourne une collection contenant les problemes
        /// </summary>
        /// <returns>Une collection de problemes</returns>
        public static List<Probleme> FetchAll()
        {
            List<Probleme> resultat = new List<Probleme>();
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _selectSql;
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Probleme probleme = new Probleme();
                string numProbleme = jeuEnregistrements["numProbleme"].ToString();
                probleme.NumProbleme = numProbleme;
                probleme.LibelleProbleme = jeuEnregistrements["libelleProbleme"].ToString();
                resultat.Add(probleme);
            }
            DataBaseAccess.Connexion.Close();
            return resultat;
        }

        #endregion
    }
}
