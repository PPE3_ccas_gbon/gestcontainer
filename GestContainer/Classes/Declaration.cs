﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Data;

namespace GestContainer.Classes
{
    /// <summary>
    /// Représente la déclaration d'un problème pour un container
    /// </summary>
    class Declaration
    {
        // Champs de la classe Declaration :

        public int NumDeclaration { get; private set; }
        public string Commentaire { get; set; }
        public DateTime DateDeclaration { get; set; }
        public bool Urgence { get; set; }
        public bool Traite { get; set; }
        public Probleme Probleme { get; set; }
        public Container Container { get; set; }

        // Champs contenant les requêtes :

        private static string _selectSql = "SELECT * FROM DECLARATION";
        private static string _selectByIdSql = "SELECT * FROM DECLARATION WHERE numDeclaration = ?numDeclaration ";
        private static string _updateSql = "UPDATE DECLARATION SET commentaire = ?commentaire, dateDeclaration = ?dateDeclaration , urgence = ?urgence, traite = ?traite, probleme = ?probleme, container = ?container  WHERE numDeclaration = ?numDeclaration ";
        private static string _insertSql = "INSERT INTO DECLARATION (commentaire, dateDeclaration, urgence, traite, probleme, container) VALUES (?commentaire, ?dateDeclaration , ?urgence, ?traite, ?probleme, ?container)";
        private static string _deleteByIdSql = "DELETE FROM DECLARATION WHERE numDeclaration = ?numDeclaration";
        private static string _getLastInsertId = "SELECT numDeclaration FROM DECLARATION WHERE commentaire = ?commentaire AND dateDeclaration = ?dateDeclaration AND urgence = ?urgence AND traite = ?traite AND probleme = ?probleme AND container = ?container";

        // Constructeur de la classe Declaration :

        /// <summary>
        /// Initialise un objet de type Declaration
        /// </summary>
        public Declaration()
        {
            NumDeclaration = -1;//Identifie une Declaration non référencé dans la base de données
        }

        // Méthodes de la classe Declaration :

        public override string ToString()
        {
            return String.Format("Commentaire : {0} DateDeclaration : {1} Urgence : {2} Traite : {3} Probleme {4} Container {5}", DateDeclaration, Urgence, Traite, Probleme, Container);
        }

        #region Méthodes d'accès aux données

        /// <summary>
        /// Valorise un objet Declaration depuis le système de gestion de bases de données
        /// </summary>
        /// <param name="numContainer">La valeur de la clé primaire</param>
        public static Declaration Fetch(int numDeclaration)
        {
            Declaration d = null;
            DataBaseAccess.Connexion.Open();//connexion à la bd

            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _selectByIdSql;//Définit la requete à utiliser
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numDeclaration", numDeclaration));//Transmet un paramètre à utiliser lors de l'envoie de la requête
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                d = new Declaration();//Initialisation de la variable Declaration
                d.NumDeclaration = Convert.ToInt32(jeuEnregistrements["NumDeclaration"].ToString());//Lecture d'un champ de l'enregistrement
                d.Commentaire = jeuEnregistrements["Commentaire"].ToString();
                d.DateDeclaration = Convert.ToDateTime(jeuEnregistrements["DateDeclaration"].ToString());
                d.Urgence = Convert.ToBoolean(jeuEnregistrements["Urgence"].ToString());
                d.Traite = Convert.ToBoolean(jeuEnregistrements["Traite"].ToString());
                //d.Probleme = (jeuEnregistrements["Urgence"].ToString()) as Probleme;
                //d.Container = Convert.
            }
            DataBaseAccess.Connexion.Close();//fermeture de la connexion
            return d;
        }

        /// <summary>
        /// Sauvegarde ou met à jour une declaration dans la base de données
        /// </summary>
        public void Save()
        {
            if (NumDeclaration == -1)
            {
                Insert();
            }
            else
            {
                Update();
            }
        }

        /// <summary>
        /// Supprime la declaration représentée par l'instance courante dans le SGBD
        /// </summary>
        public void Delete()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numDeclaration", NumDeclaration));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            NumDeclaration = -1;
        }

        /// <summary>
        /// Met à jour la declaration représentée par l'instance courante dans le SGBD
        /// </summary>
        private void Update()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numDeclaration", NumDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?commentaire", Commentaire));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateDeclaration", DateDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?urgence", Urgence));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?traite", Traite));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?probleme", Probleme));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?container", Container));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Insère la déclaration de l'instance courante dans le SGBD
        /// </summary>
        private void Insert()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numDeclaration", NumDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?commentaire", Commentaire));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateDeclaration", DateDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?urgence", Urgence));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?traite", Traite));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?probleme", Probleme));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?container", Container));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            IDbCommand commandGetLastId = DataBaseAccess.Connexion.CreateCommand();
            commandGetLastId.CommandText = _getLastInsertId;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numDeclaration", NumDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?commentaire", Commentaire));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateDeclaration", DateDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?urgence", Urgence));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?traite", Traite));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?probleme", Probleme));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?container", Container));
            commandGetLastId.Prepare();
            IDataReader jeuEnregistrements = commandGetLastId.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                NumDeclaration = Convert.ToInt32(jeuEnregistrements["numDeclaration"].ToString());
            }
            else
            {
                commandSql.Transaction.Rollback();
                throw new Exception("Duplicate entry");
            }
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Retourne une collection contenant les declarations
        /// </summary>
        /// <returns>Une collection de declarations</returns>
        public static List<Declaration> FetchAll()
        {
            List<Declaration> resultat = new List<Declaration>();
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _selectSql;
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Declaration declaration = new Declaration();
                string numDeclaration = jeuEnregistrements["numDeclaration"].ToString();
                declaration.NumDeclaration = Convert.ToInt32(numDeclaration);
                declaration.Commentaire = jeuEnregistrements["TypeContainer"].ToString();
                declaration.DateDeclaration = Convert.ToDateTime(jeuEnregistrements["DateDeclaration"].ToString());
                declaration.Urgence = Convert.ToBoolean(jeuEnregistrements["Urgence"].ToString());
                declaration.Traite = Convert.ToBoolean(jeuEnregistrements["Traite"].ToString());
                //declaration.Probleme = jeuEnregistrements["Probleme"].ToString();
                //declaration.Container = jeuEnregistrements["Container"].ToString();
                resultat.Add(declaration);
            }
            DataBaseAccess.Connexion.Close();
            return resultat;
        }

        #endregion

    }
}
