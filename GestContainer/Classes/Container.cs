﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Data;

namespace GestContainer
{
    /// <summary>
    /// Représente un container dans la BDD
    /// </summary>
    class Container
    {
        // Champs de la classe Container :

        public int NumContainer { get; set; }
        public DateTime DateAchat { get; set; }
        public DateTime DateLimiteInspection { get; set; }
        public String TypeContainer { get; set; }

        // Champs contenant les requêtes :
        private static string _selectSql = "SELECT * FROM CONTAINER";
        private static string _selectByIdSql ="SELECT * FROM CONTAINER WHERE numContainer = ?numContainer ";
        private static string _updateSql = "UPDATE CONTAINER SET numContainer = ?numContainer, typeContainer = ?typeContainer, dateAchat=?dateAchat , dateLimiteInspection=?dateLimiteInspection WHERE numContainer=?numContainer ";
        private static string _insertSql = "INSERT INTO CONTAINER (numContainer,typeContainer,dateAchat,dateLimiteInspection) VALUES (?numContainer,?typeContainer,?dateAchat,?dateLimiteContainer)";
        private static string _deleteByIdSql = "DELETE FROM CONTAINER WHERE numContainer = ?numContainer";
        private static string _getLastInsertId = "SELECT numContainer FROM CONTAINER WHERE typeContainer = ?typeContainer AND dateAchat=?dateAchat AND dateLimiteInspection=?dateLimiteInspection";

        // Constructeur de la classe Container :

        /// <summary>
        /// Initialise un objet de type Container
        /// </summary>
        public Container()
        {
            NumContainer = -1;
        }

        // Methodes de la classe Container : 

        public override string ToString()
        {
            return String.Format("DateAchat : {0} DateLimiteInspection : {1} TypeContainer : {2}", DateAchat, DateLimiteInspection, TypeContainer);
        }

        #region Méthodes d'accès aux données

        /// <summary>
        /// Valorise un objet container depuis le système de gestion de bases de données
        /// </summary>
        /// <param name="numContainer">La valeur de la clé primaire</param>
        public static Container Fetch(int numContainer)
        {
            Container c = null;
            DataBaseAccess.Connexion.Open();//connection à la bd

            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _selectByIdSql;//Définit la requete à utiliser
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numContainer", numContainer));//Transmet un paramètre à utiliser lors de l'envoie de la requête
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                c = new Container();//Initialisation de la variable Container
                c.NumContainer = Convert.ToInt16(jeuEnregistrements["numContainer"].ToString());//Lecture d'un champ de l'enregistrement
                c.TypeContainer = jeuEnregistrements["typeContainer"].ToString();
                c.DateAchat = Convert.ToDateTime(jeuEnregistrements["dateAchat"].ToString());
                c.DateLimiteInspection = Convert.ToDateTime(jeuEnregistrements["dateLimiteInspection"].ToString());
            }
            DataBaseAccess.Connexion.Close();//fermeture de la connexion
            return c;
        }

        /// <summary>
        /// Sauvegarde ou met à jour un container dans la base de données
        /// </summary>
        public void Save()
        {
            if (NumContainer == -1)
            {
                Insert();
            }
            else
            {
                Update();
            }
        }

        /// <summary>
        /// Supprime le container représenté par l'instance courante dans le SGBD
        /// </summary>
        public void Delete()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numContainer", NumContainer));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            NumContainer = -1;
        }

        /// <summary>
        /// Met à jour le container représenté par l'instance courante dans le SGBD
        /// </summary>
        private void Update()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numContainer", NumContainer));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?typeContainer", TypeContainer));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateAchat", DateAchat));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateLimiteInspection", DateLimiteInspection));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Insère le container de l'instance courante dans le SGBD
        /// </summary>
        private void Insert()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numContainer", NumContainer));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?typeContainer", TypeContainer));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateAchat", DateAchat));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateLimiteInspection", DateLimiteInspection));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            IDbCommand commandGetLastId = DataBaseAccess.Connexion.CreateCommand();
            commandGetLastId.CommandText = _getLastInsertId;
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?typeContainer", TypeContainer));
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?dateAchat", DateAchat));
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?dateLimiteInspection", DateLimiteInspection));
            commandGetLastId.Prepare();
            IDataReader jeuEnregistrements = commandGetLastId.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                NumContainer = Convert.ToInt16(jeuEnregistrements["numContainer"].ToString());
            }
            else
            {
                commandSql.Transaction.Rollback();
                throw new Exception("Duplicate entry");
            }
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Retourne une collection contenant les containers
        /// </summary>
        /// <returns>Une collection de containers</returns>
        public static List<Container> FetchAll()
        {
            List<Container> resultat = new List<Container>();
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _selectSql;
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Container container = new Container();
                string numContainer = jeuEnregistrements["numContainer"].ToString();
                container.NumContainer = Convert.ToInt16(numContainer);
                container.TypeContainer = jeuEnregistrements["typeContainer"].ToString();
                container.DateAchat = Convert.ToDateTime(jeuEnregistrements["dateAchat"].ToString());
                container.DateLimiteInspection = Convert.ToDateTime(jeuEnregistrements["dateLimiteInspection"].ToString());
                resultat.Add(container);
            }
            DataBaseAccess.Connexion.Close();
            return resultat;
        }

        #endregion
    }
}
