﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Data;

namespace GestContainer.Classes
{
    /// <summary>
    /// Représente un travail à effectuer sur un container suite à une inspection
    /// </summary>
    class Travaux
    {
        // Champs de la classe Travaux :

        public string CodeTravaux { get; private set; }
        public string LibelleTravaux { get; set; }
        public int DureeImmobilisation { get; set; }

        // Champs contenant les requêtes :

        private static string _selectSql = "SELECT * FROM TRAVAUX";
        private static string _selectByIdSql = "SELECT * FROM TRAVAUX WHERE codeTravaux = ?codeTravaux ";
        private static string _updateSql = "UPDATE TRAVAUX SET libelleTravaux = ?libelleTravaux, dureeImmobilisation=?dureeImmobilisation WHERE codeTravaux=?codeTravaux ";
        private static string _insertSql = "INSERT INTO TRAVAUX (libelleTravaux,dureeImmobilisation) VALUES (?libelleTravaux,?dureeImmobilisation)";
        private static string _deleteByIdSql = "DELETE FROM TRAVAUX WHERE codeTravaux = ?codeTravaux";
        private static string _getLastInsertId = "SELECT codeTravaux FROM TRAVAUX WHERE libelleTravaux = ?libelleTravaux AND dureeImmobilisation=?dureeImmobilisation";

        // Constructeur de la classe Travaux :

        /// <summary>
        /// Initialise un objet de type Travaux
        /// </summary>
        public Travaux()
        {
            CodeTravaux = "-1";//Identifie un travaux non référencé dans la base de données
        }

        // Méthodes de la classe Travaux :

        public override string ToString()
        {
            return String.Format("LibelleTravaux : {0} DureeImmobilisation : {1}", LibelleTravaux, DureeImmobilisation);
        }

        #region Méthodes d'accès aux données

        /// <summary>
        /// Valorise un objet travaux depuis le système de gestion de bases de données
        /// </summary>
        /// <param name="codeTravaux">La valeur de la clé primaire</param>
        public static Travaux Fetch(int codeTravaux)
        {
            Travaux t = null;
            DataBaseAccess.Connexion.Open();//connection à la bd

            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _selectByIdSql;//Définit la requete à utiliser
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeTravaux", codeTravaux));//Transmet un paramètre à utiliser lors de l'envoie de la requête
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                t = new Travaux();//Initialisation de la variable travaux
                t.CodeTravaux = jeuEnregistrements["codeTravaux"].ToString();//Lecture d'un champ de l'enregistrement
                t.LibelleTravaux = jeuEnregistrements["libelleTravaux"].ToString();
                t.DureeImmobilisation = Convert.ToInt16(jeuEnregistrements["dureeImmobilisation"].ToString());
            }
            DataBaseAccess.Connexion.Close();//fermeture de la connexion
            return t;
        }

        /// <summary>
        /// Sauvegarde ou met à jour un travail dans la base de données
        /// </summary>
        public void Save()
        {
            if (CodeTravaux == "-1")
            {
                Insert();
            }
            else
            {
                Update();
            }
        }

        /// <summary>
        /// Supprime le travail représenté par l'instance courante dans le SGBD
        /// </summary>
        public void Delete()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeTravaux", CodeTravaux));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            CodeTravaux = "-1";
        }

        /// <summary>
        /// Met à jour le travail représenté par l'instance courante dans le SGBD
        /// </summary>
        private void Update()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeTravaux", CodeTravaux));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?libelleTravaux", LibelleTravaux));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dureeimmobilisation", DureeImmobilisation));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Insère le travail représenté par l'instance courante dans le SGBD
        /// </summary>
        private void Insert()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?libelleTravaux", LibelleTravaux));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dureeImmobilisation", DureeImmobilisation));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            IDbCommand commandGetLastId = DataBaseAccess.Connexion.CreateCommand();
            commandGetLastId.CommandText = _getLastInsertId;
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?libelleTravaux", LibelleTravaux));
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?dureeImmobilisation", DureeImmobilisation));
            commandGetLastId.Prepare();
            IDataReader jeuEnregistrements = commandGetLastId.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                CodeTravaux = jeuEnregistrements["codeTravaux"].ToString();
            }
            else
            {
                commandSql.Transaction.Rollback();
                throw new Exception("Duplicate entry");
            }
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Retourne une collection contenant les travaux
        /// </summary>
        /// <returns>Une collection de travaux</returns>
        public static List<Travaux> FetchAll()
        {
            List<Travaux> resultat = new List<Travaux>();
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _selectSql;
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Travaux travaux = new Travaux();
                string codeTravaux = jeuEnregistrements["codeTravaux"].ToString();
                travaux.CodeTravaux = codeTravaux;
                travaux.LibelleTravaux = jeuEnregistrements["libelleTravaux"].ToString();
                travaux.DureeImmobilisation = Convert.ToInt16(jeuEnregistrements["dureeImmobilisation"].ToString());
                resultat.Add(travaux);
            }
            DataBaseAccess.Connexion.Close();
            return resultat;
        }

        #endregion
    }
}
