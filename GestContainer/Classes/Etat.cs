﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Data;

namespace GestContainer
{
    /// <summary>
    /// Représente l'état d'un container
    /// </summary>
    class Etat
    {
        // Champs de la classe Etat :

        public int CodeEtat { get; set; }
        public string LibelleEtat { get; set; }

        // Champs contenant les requêtes :

        private static string _selectSql = "SELECT * FROM ETAT";
        private static string _selectByIdSql = "SELECT * FROM ETAT WHERE codeEtat = ?codeEtat ";
        private static string _updateSql = "UPDATE ETAT SET libelleEtat = ?libelleEtat WHERE codeEtat=?codeEtat ";
        private static string _insertSql = "INSERT INTO ETAT (libelleEtat) VALUES (?libelleEtat)";
        private static string _deleteByIdSql = "DELETE FROM ETAT WHERE codeEtat = ?codeEtat";
        private static string _getLastInsertId = "SELECT codeEtat FROM ETAT WHERE libelleEtat = ?libelleEtat";

        // Constructeur de la classe Etat :

        /// <summary>
        /// Initialise un objet de type Etat
        /// </summary>
        public Etat()
        {
            CodeEtat = -1;//Identifie un Etat non référencé dans la base de données
        }

        // Méthodes de la classe Etat :

        public override string ToString()
        {
            return String.Format("LibelleEtat : {0} ", LibelleEtat);
        }

        #region Méthodes d'accès aux données

        /// <summary>
        /// Valorise un objet etat depuis le système de gestion de bases de données
        /// </summary>
        /// <param name="codeEtat">La valeur de la clé primaire</param>
        public static Etat Fetch(int codeEtat)
        {
            Etat e = null;
            DataBaseAccess.Connexion.Open();//connection à la bd

            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _selectByIdSql;//Définit la requete à utiliser
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeEtat", codeEtat));//Transmet un paramètre à utiliser lors de l'envoie de la requête
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                e = new Etat();//Initialisation de la variable etat
                e.CodeEtat = Convert.ToInt16(jeuEnregistrements["codeEtat"].ToString());//Lecture d'un champ de l'enregistrement
                e.LibelleEtat = jeuEnregistrements["libelleEtat"].ToString();
            }
            DataBaseAccess.Connexion.Close();//fermeture de la connexion
            return e;
        }

        /// <summary>
        /// Sauvegarde ou met à jour un etat dans la base de données
        /// </summary>
        public void Save()
        {
            if (CodeEtat == -1)
            {
                Insert();
            }
            else
            {
                Update();
            }
        }

        /// <summary>
        /// Supprime l'etat représenté par l'instance courante dans le SGBD
        /// </summary>
        public void Delete()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeEtat", CodeEtat));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            CodeEtat = -1;
        }

        /// <summary>
        /// Met à jour l'instance courante dans le SGBD
        /// </summary>
        private void Update()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeEtat", CodeEtat));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?libelleEtat", LibelleEtat));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Insère l'état de l'instance courante dans la BDD
        /// </summary>
        private void Insert()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?libelleEtat", LibelleEtat));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            IDbCommand commandGetLastId = DataBaseAccess.Connexion.CreateCommand();
            commandGetLastId.CommandText = _getLastInsertId;
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?libelleEtat", LibelleEtat));
            commandGetLastId.Prepare();
            IDataReader jeuEnregistrements = commandGetLastId.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                CodeEtat = Convert.ToInt16(jeuEnregistrements["codeEtat"].ToString());
            }
            else
            {
                commandSql.Transaction.Rollback();
                throw new Exception("Duplicate entry");
            }
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Retourne une collection contenant les etats
        /// </summary>
        /// <returns>Une collection d'etats</returns>
        public static List<Etat> FetchAll()
        {
            List<Etat> resultat = new List<Etat>();
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _selectSql;
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Etat etat = new Etat();
                string codeEtat = jeuEnregistrements["codeEtat"].ToString();
                etat.CodeEtat = Convert.ToInt16(codeEtat);
                etat.LibelleEtat = jeuEnregistrements["libelleEtat"].ToString();
                resultat.Add(etat);
            }
            DataBaseAccess.Connexion.Close();
            return resultat;
        }

        #endregion
    }
}
