﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Data;

namespace GestContainer
{
    /// <summary>
    /// Représente une inspection
    /// </summary>
    class Inspection
    {
        // Champs de la classe Inspection :

        private int NumContainer { get; set; }
        private int NumInspection { get; set; }
        private int CodeMotif { get; set; }
        private int CodeEtat { get; set; }
        private DateTime DateInspection { get; set; }
        private string CommentairePostInspection { get; set; }
        private string Avis { get; set; }
        private List<Container> LesContainers { get; set; }
        private List<Motif> LesMotifs { get; set; }
        private List<Etat> LesEtats { get; set; }

        // Champs contenant les requêtes :

        private static string _selectSql = "SELECT * FROM INSPECTION";
        private static string _selectByIdSql = "SELECT * FROM INSPECTION WHERE numContainer = ?numContainer AND numInspection = ?numInspection";
        private static string _updateSql = "UPDATE INSPECTION SET codeMotif = ?codeMotif, codeEtat=?codeEtat , dateInspection=?dateInspection  WHERE numContainer=?numContainer AND numInspection=?numInspection ";
        private static string _insertSql = "INSERT INTO INSPECTION (codeMotif, codeEtat,  dateInspection, CommentairePostInspection, avis) VALUES (?codeMotif, ?codeEtat,  ?dateInspection, ?CommentairePostInspection, ?avis)";
        private static string _deleteByIdSql = "DELETE FROM INSPECTION WHERE numContainer = ?numContainer AND numInspection = ?numInspection";
        private static string _getLastInsertId = "SELECT numContainer, numInspection FROM INSPECTION WHERE codeMotif = ?codeMotif AND codeEtat=?codeEtat AND dateInspection=?dateInspection AND CommentairePostInspection=?CommentairePostInspection AND avis=?avis";

        // Constructeur de la classe Inspection :

        /// <summary>
        /// Initialise un objet de type Inspection
        /// </summary>
        public Inspection()
        {
            NumContainer = -1;//Identifie une Inspection non référencée dans la base de données
        }

        // Méthodes de la classe Inspection :

        public override string ToString()
        {
            return String.Format("CodeMotif : {0} CodeEtat : {1} DateInspection : {2} CommentairePostInspection : {3} Avis : {4} LesContainers : {5} LesMotifs : {4} LesEtats {6}", CodeMotif, CodeEtat, DateInspection, CommentairePostInspection, Avis, LesContainers, LesMotifs, LesEtats);
        }

        #region Méthodes d'accès aux données

        /// <summary>
        /// Valorise un objet Inspection depuis le système de gestion de bases de données
        /// </summary>
        /// <param name="numContainer, numInspection">La valeur de la clé primaire</param>
        public static Inspection Fetch(int numContainer, int numInspection)
        {
            Inspection i = null;
            DataBaseAccess.Connexion.Open();//connexion à la bd

            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _selectByIdSql;//Définit la requete à utiliser
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numContainer", numContainer));//Transmet un paramètre à utiliser lors de l'envoie de la requête
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numInspection", numInspection));//Transmet un paramètre à utiliser lors de l'envoie de la requête
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                i = new Inspection();//Initialisation de la variable inspection
                i.NumContainer = Convert.ToInt32(jeuEnregistrements["NumContainer"].ToString());//Lecture d'un champ de l'enregistrement
                i.NumInspection = Convert.ToInt32(jeuEnregistrements["NumInspection"].ToString());
                i.CodeMotif = Convert.ToInt32(jeuEnregistrements["CodeMotif"].ToString());
                i.CodeEtat = Convert.ToInt32(jeuEnregistrements["CodeEtat"].ToString());
                i.DateInspection = Convert.ToDateTime(jeuEnregistrements["DateInspection"].ToString());
                i.CommentairePostInspection = jeuEnregistrements["TypeContainer"].ToString();
                i.Avis = jeuEnregistrements["TypeContainer"].ToString();
            }
            DataBaseAccess.Connexion.Close();//fermeture de la connexion
            return i;
        }

        /// <summary>
        /// Sauvegarde ou met à jour une inspection dans la base de données
        /// </summary>
        public void Save()
        {
            if (NumInspection == -1)
            {
                Insert();
            }
            else
            {
                Update();
            }
        }

        /// <summary>
        /// Supprime l'inspection représenté par l'instance courante dans le SGBD
        /// </summary>
        public void Delete()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?NumInspection", NumInspection));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            NumInspection = -1;
        }

        /// <summary>
        /// Met à jour l'inspection de l'instance courante dans le SGBD
        /// </summary>
        private void Update()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?NumInspection", NumInspection));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?NumContainer", NumContainer));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeMotif", CodeMotif));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeEtat", CodeEtat));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateInspection", DateInspection));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?CommentairePostInspection", CommentairePostInspection));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?avis", Avis));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Insère l'inspection de l'instance courante dans le SGBD
        /// </summary>
        private void Insert()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeMotif", CodeMotif));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeEtat", CodeEtat));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateInspection", DateInspection));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?CommentairePostInspection", CommentairePostInspection));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?avis", Avis));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            IDbCommand commandGetLastId = DataBaseAccess.Connexion.CreateCommand();
            commandGetLastId.CommandText = _getLastInsertId;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeMotif", CodeMotif));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeEtat", CodeEtat));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateInspection", DateInspection));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?CommentairePostInspection", CommentairePostInspection));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?avis", Avis));
            commandGetLastId.Prepare();
            IDataReader jeuEnregistrements = commandGetLastId.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                NumInspection = Convert.ToInt32(jeuEnregistrements["NumInspection"].ToString());
            }
            else
            {
                commandSql.Transaction.Rollback();
                throw new Exception("Duplicate entry");
            }
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Retourne une collection contenant les inspections
        /// </summary>
        /// <returns>Une collection d'inspections</returns>
        public static List<Inspection> FetchAll()
        {
            List<Inspection> resultat = new List<Inspection>();
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _selectSql;
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Inspection inspection = new Inspection();
                string NumInspection = jeuEnregistrements["NumInspection"].ToString();
                inspection.NumInspection = Convert.ToInt32(NumInspection);
                inspection.NumContainer = Convert.ToInt32(jeuEnregistrements["NumContainer"].ToString());
                inspection.CodeMotif = Convert.ToInt32(jeuEnregistrements["CodeMotif"].ToString());
                inspection.CodeEtat = Convert.ToInt32(jeuEnregistrements["CodeEtat"].ToString());
                inspection.DateInspection = Convert.ToDateTime(jeuEnregistrements["DateInspection"].ToString());
                inspection.CommentairePostInspection = jeuEnregistrements["TypeContainer"].ToString();
                inspection.Avis = jeuEnregistrements["TypeContainer"].ToString();
                resultat.Add(inspection);
            }
            DataBaseAccess.Connexion.Close();
            return resultat;
        }

        #endregion
        
    }
}
