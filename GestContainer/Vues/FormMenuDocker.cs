﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestContainer
{
    public partial class FormMenuDocker : Form
    {
        private Form _mdiChild;

        private Form MdiChild
        {
            get { return _mdiChild; }
            set
            {
                if (_mdiChild != null)
                {
                    _mdiChild.Dispose();
                }
                _mdiChild = value;
                _mdiChild.MaximumSize = _mdiChild.Size;
                _mdiChild.MinimumSize = _mdiChild.Size;
                _mdiChild.Show();
            }
        }
        public FormMenuDocker()
        {
            InitializeComponent();
        }

        private void SaisirInspectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormSaisieInspection();
        }

        private void ConsulterInspectionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormConsulterInspection();
        }

        private void DeclarerProblemeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormConsulterProbleme();
        }

        private void FormMenuDocker_Load(object sender, EventArgs e)
        {

        }
    }
}
