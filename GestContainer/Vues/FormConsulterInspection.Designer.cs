﻿namespace GestContainer
{
    partial class FormConsulterInspection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConsulterInspection));
            this.Button2 = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.DataGridViewInspecFinies = new System.Windows.Forms.DataGridView();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.DataGridViewInspecEnCours = new System.Windows.Forms.DataGridView();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.DataGridViewInspecPrevues = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.GroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewInspecFinies)).BeginInit();
            this.GroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewInspecEnCours)).BeginInit();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewInspecPrevues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Button2
            // 
            this.Button2.Location = new System.Drawing.Point(680, 371);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(176, 23);
            this.Button2.TabIndex = 8;
            this.Button2.Text = "L\'inspection sélectionnée est finie";
            this.Button2.UseVisualStyleBackColor = true;
            // 
            // Button1
            // 
            this.Button1.Location = new System.Drawing.Point(229, 371);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(195, 23);
            this.Button1.TabIndex = 7;
            this.Button1.Text = "L\'inspection sélectionnée est en cours";
            this.Button1.UseVisualStyleBackColor = true;
            // 
            // GroupBox3
            // 
            this.GroupBox3.Controls.Add(this.DataGridViewInspecFinies);
            this.GroupBox3.Font = new System.Drawing.Font("Calibri", 15.25F, System.Drawing.FontStyle.Bold);
            this.GroupBox3.Location = new System.Drawing.Point(862, 83);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(419, 277);
            this.GroupBox3.TabIndex = 5;
            this.GroupBox3.TabStop = false;
            this.GroupBox3.Text = "Inspections finies";
            // 
            // DataGridViewInspecFinies
            // 
            this.DataGridViewInspecFinies.AllowUserToAddRows = false;
            this.DataGridViewInspecFinies.AllowUserToDeleteRows = false;
            this.DataGridViewInspecFinies.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewInspecFinies.Location = new System.Drawing.Point(6, 31);
            this.DataGridViewInspecFinies.Name = "DataGridViewInspecFinies";
            this.DataGridViewInspecFinies.ReadOnly = true;
            this.DataGridViewInspecFinies.Size = new System.Drawing.Size(406, 240);
            this.DataGridViewInspecFinies.TabIndex = 0;
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.DataGridViewInspecEnCours);
            this.GroupBox2.Font = new System.Drawing.Font("Calibri", 15.25F, System.Drawing.FontStyle.Bold);
            this.GroupBox2.Location = new System.Drawing.Point(437, 83);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(419, 277);
            this.GroupBox2.TabIndex = 6;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Inspections en cours";
            // 
            // DataGridViewInspecEnCours
            // 
            this.DataGridViewInspecEnCours.AllowUserToAddRows = false;
            this.DataGridViewInspecEnCours.AllowUserToDeleteRows = false;
            this.DataGridViewInspecEnCours.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewInspecEnCours.Location = new System.Drawing.Point(6, 31);
            this.DataGridViewInspecEnCours.Name = "DataGridViewInspecEnCours";
            this.DataGridViewInspecEnCours.ReadOnly = true;
            this.DataGridViewInspecEnCours.Size = new System.Drawing.Size(406, 240);
            this.DataGridViewInspecEnCours.TabIndex = 0;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.DataGridViewInspecPrevues);
            this.GroupBox1.Font = new System.Drawing.Font("Calibri", 15.25F, System.Drawing.FontStyle.Bold);
            this.GroupBox1.Location = new System.Drawing.Point(5, 83);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(419, 277);
            this.GroupBox1.TabIndex = 4;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Inspections prévues";
            // 
            // DataGridViewInspecPrevues
            // 
            this.DataGridViewInspecPrevues.AllowUserToAddRows = false;
            this.DataGridViewInspecPrevues.AllowUserToDeleteRows = false;
            this.DataGridViewInspecPrevues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewInspecPrevues.Location = new System.Drawing.Point(6, 31);
            this.DataGridViewInspecPrevues.Name = "DataGridViewInspecPrevues";
            this.DataGridViewInspecPrevues.ReadOnly = true;
            this.DataGridViewInspecPrevues.Size = new System.Drawing.Size(406, 240);
            this.DataGridViewInspecPrevues.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(521, -7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(260, 94);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // FormConsulterInspection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1289, 406);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.GroupBox3);
            this.Controls.Add(this.GroupBox2);
            this.Controls.Add(this.GroupBox1);
            this.Name = "FormConsulterInspection";
            this.Text = "FormConsulterInspection";
            this.Load += new System.EventHandler(this.FormConsulterInspection_Load);
            this.GroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewInspecFinies)).EndInit();
            this.GroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewInspecEnCours)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewInspecPrevues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.DataGridView DataGridViewInspecFinies;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.DataGridView DataGridViewInspecEnCours;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.DataGridView DataGridViewInspecPrevues;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}