﻿namespace GestContainer
{
    partial class FormAuthentification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAuthentification));
            this.GroupBox_Authentification = new System.Windows.Forms.GroupBox();
            this.Label_Identifiant = new System.Windows.Forms.Label();
            this.Button_Valider = new System.Windows.Forms.Button();
            this.TextBoxMDP = new System.Windows.Forms.TextBox();
            this.Label_MotDePasse = new System.Windows.Forms.Label();
            this.TextBoxIdentifiant = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.GroupBox_Authentification.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupBox_Authentification
            // 
            this.GroupBox_Authentification.BackColor = System.Drawing.SystemColors.Control;
            this.GroupBox_Authentification.Controls.Add(this.Label_Identifiant);
            this.GroupBox_Authentification.Controls.Add(this.Button_Valider);
            this.GroupBox_Authentification.Controls.Add(this.TextBoxMDP);
            this.GroupBox_Authentification.Controls.Add(this.Label_MotDePasse);
            this.GroupBox_Authentification.Controls.Add(this.TextBoxIdentifiant);
            this.GroupBox_Authentification.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox_Authentification.ForeColor = System.Drawing.Color.CadetBlue;
            this.GroupBox_Authentification.Location = new System.Drawing.Point(12, 94);
            this.GroupBox_Authentification.Name = "GroupBox_Authentification";
            this.GroupBox_Authentification.Size = new System.Drawing.Size(385, 244);
            this.GroupBox_Authentification.TabIndex = 6;
            this.GroupBox_Authentification.TabStop = false;
            this.GroupBox_Authentification.Text = "Authentification";
            this.GroupBox_Authentification.Enter += new System.EventHandler(this.GroupBox_Authentification_Enter);
            // 
            // Label_Identifiant
            // 
            this.Label_Identifiant.AutoSize = true;
            this.Label_Identifiant.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.Label_Identifiant.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_Identifiant.Location = new System.Drawing.Point(43, 68);
            this.Label_Identifiant.Name = "Label_Identifiant";
            this.Label_Identifiant.Size = new System.Drawing.Size(95, 25);
            this.Label_Identifiant.TabIndex = 0;
            this.Label_Identifiant.Text = "Identifiant";
            // 
            // Button_Valider
            // 
            this.Button_Valider.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.Button_Valider.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Button_Valider.Location = new System.Drawing.Point(145, 186);
            this.Button_Valider.Name = "Button_Valider";
            this.Button_Valider.Size = new System.Drawing.Size(88, 36);
            this.Button_Valider.TabIndex = 2;
            this.Button_Valider.Text = "Valider";
            this.Button_Valider.UseVisualStyleBackColor = true;
            // 
            // TextBoxMDP
            // 
            this.TextBoxMDP.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.TextBoxMDP.Location = new System.Drawing.Point(200, 111);
            this.TextBoxMDP.Name = "TextBoxMDP";
            this.TextBoxMDP.Size = new System.Drawing.Size(147, 30);
            this.TextBoxMDP.TabIndex = 4;
            // 
            // Label_MotDePasse
            // 
            this.Label_MotDePasse.AutoSize = true;
            this.Label_MotDePasse.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.Label_MotDePasse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label_MotDePasse.Location = new System.Drawing.Point(43, 116);
            this.Label_MotDePasse.Name = "Label_MotDePasse";
            this.Label_MotDePasse.Size = new System.Drawing.Size(130, 25);
            this.Label_MotDePasse.TabIndex = 1;
            this.Label_MotDePasse.Text = "Mot de passe";
            // 
            // TextBoxIdentifiant
            // 
            this.TextBoxIdentifiant.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.TextBoxIdentifiant.Location = new System.Drawing.Point(200, 63);
            this.TextBoxIdentifiant.Name = "TextBoxIdentifiant";
            this.TextBoxIdentifiant.Size = new System.Drawing.Size(147, 30);
            this.TextBoxIdentifiant.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(78, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(260, 94);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // FormAuthentification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(411, 349);
            this.Controls.Add(this.GroupBox_Authentification);
            this.Controls.Add(this.pictureBox1);
            this.Name = "FormAuthentification";
            this.Text = "FormAuthentification";
            this.Load += new System.EventHandler(this.FormAuthentification_Load);
            this.GroupBox_Authentification.ResumeLayout(false);
            this.GroupBox_Authentification.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox_Authentification;
        internal System.Windows.Forms.Label Label_Identifiant;
        internal System.Windows.Forms.Button Button_Valider;
        internal System.Windows.Forms.TextBox TextBoxMDP;
        internal System.Windows.Forms.Label Label_MotDePasse;
        internal System.Windows.Forms.TextBox TextBoxIdentifiant;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}