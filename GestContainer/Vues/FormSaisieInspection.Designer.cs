﻿namespace GestContainer
{
    partial class FormSaisieInspection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSaisieInspection));
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.TextBoxRenseignement = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.Button1 = new System.Windows.Forms.Button();
            this.Label4 = new System.Windows.Forms.Label();
            this.ComboBoxMotif = new System.Windows.Forms.ComboBox();
            this.ComboBoxEtatInspection = new System.Windows.Forms.ComboBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.DateTimePickerDateInspection = new System.Windows.Forms.DateTimePicker();
            this.Label2 = new System.Windows.Forms.Label();
            this.ComboBoxContainer = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.TextBoxRenseignement);
            this.GroupBox1.Controls.Add(this.Label5);
            this.GroupBox1.Controls.Add(this.Button1);
            this.GroupBox1.Controls.Add(this.Label4);
            this.GroupBox1.Controls.Add(this.ComboBoxMotif);
            this.GroupBox1.Controls.Add(this.ComboBoxEtatInspection);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.DateTimePickerDateInspection);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.ComboBoxContainer);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Font = new System.Drawing.Font("Calibri", 18.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox1.ForeColor = System.Drawing.Color.CadetBlue;
            this.GroupBox1.Location = new System.Drawing.Point(12, 112);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(436, 372);
            this.GroupBox1.TabIndex = 1;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Saisir une inspection";
            this.GroupBox1.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // TextBoxRenseignement
            // 
            this.TextBoxRenseignement.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxRenseignement.Location = new System.Drawing.Point(218, 145);
            this.TextBoxRenseignement.Name = "TextBoxRenseignement";
            this.TextBoxRenseignement.Size = new System.Drawing.Size(200, 33);
            this.TextBoxRenseignement.TabIndex = 10;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.Label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label5.Location = new System.Drawing.Point(21, 149);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(193, 25);
            this.Label5.TabIndex = 9;
            this.Label5.Text = "Autre renseignement";
            // 
            // Button1
            // 
            this.Button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.Button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Button1.Location = new System.Drawing.Point(168, 320);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(96, 34);
            this.Button1.TabIndex = 8;
            this.Button1.Text = "Valider";
            this.Button1.UseVisualStyleBackColor = true;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.Label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label4.Location = new System.Drawing.Point(10, 101);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(182, 25);
            this.Label4.TabIndex = 7;
            this.Label4.Text = "Motif de l\'inspection";
            // 
            // ComboBoxMotif
            // 
            this.ComboBoxMotif.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboBoxMotif.FormattingEnabled = true;
            this.ComboBoxMotif.Location = new System.Drawing.Point(218, 98);
            this.ComboBoxMotif.Name = "ComboBoxMotif";
            this.ComboBoxMotif.Size = new System.Drawing.Size(200, 34);
            this.ComboBoxMotif.TabIndex = 6;
            // 
            // ComboBoxEtatInspection
            // 
            this.ComboBoxEtatInspection.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboBoxEtatInspection.FormattingEnabled = true;
            this.ComboBoxEtatInspection.Location = new System.Drawing.Point(218, 244);
            this.ComboBoxEtatInspection.Name = "ComboBoxEtatInspection";
            this.ComboBoxEtatInspection.Size = new System.Drawing.Size(200, 34);
            this.ComboBoxEtatInspection.TabIndex = 5;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.Label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label3.Location = new System.Drawing.Point(21, 244);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(174, 25);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "Etat de l\'inspection";
            // 
            // DateTimePickerDateInspection
            // 
            this.DateTimePickerDateInspection.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTimePickerDateInspection.Location = new System.Drawing.Point(218, 197);
            this.DateTimePickerDateInspection.Name = "DateTimePickerDateInspection";
            this.DateTimePickerDateInspection.Size = new System.Drawing.Size(200, 26);
            this.DateTimePickerDateInspection.TabIndex = 3;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.Label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label2.Location = new System.Drawing.Point(16, 198);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(181, 25);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "Date de l\'inspection";
            // 
            // ComboBoxContainer
            // 
            this.ComboBoxContainer.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboBoxContainer.FormattingEnabled = true;
            this.ComboBoxContainer.Location = new System.Drawing.Point(218, 46);
            this.ComboBoxContainer.Name = "ComboBoxContainer";
            this.ComboBoxContainer.Size = new System.Drawing.Size(200, 34);
            this.ComboBoxContainer.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.Label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Label1.Location = new System.Drawing.Point(98, 49);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(97, 25);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Container";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(99, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(260, 94);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // FormSaisieInspection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 491);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.GroupBox1);
            this.Name = "FormSaisieInspection";
            this.Text = "FormSaisieInspection";
            this.Load += new System.EventHandler(this.FormSaisieInspection_Load);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.TextBox TextBoxRenseignement;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.ComboBox ComboBoxMotif;
        internal System.Windows.Forms.ComboBox ComboBoxEtatInspection;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.DateTimePicker DateTimePickerDateInspection;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.ComboBox ComboBoxContainer;
        internal System.Windows.Forms.Label Label1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}