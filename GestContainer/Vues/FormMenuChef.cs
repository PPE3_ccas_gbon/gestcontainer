﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestContainer
{
    public partial class FormMenuChef : Form
    {
        private Form _mdiChild;

        private Form MdiChild
        {
            get { return _mdiChild; }
            set
            {
                if (_mdiChild != null)
                {
                    _mdiChild.Dispose();
                }
                _mdiChild = value;
                _mdiChild.MaximumSize = _mdiChild.Size;
                _mdiChild.MinimumSize = _mdiChild.Size;
                _mdiChild.Show();
            }
        }

        public FormMenuChef()
        {
            InitializeComponent();
        }

        private void FormMenuChef_Load(object sender, EventArgs e)
        {

        }

        private void InspectionToolStripMenuItemSaisieInspection_Click(object sender, EventArgs e)
        {
            MdiChild = new FormSaisieInspection();
        }

        private void InspectionsToolStripMenuItemConsulterInsp_Click(object sender, EventArgs e)
        {
            MdiChild = new FormConsulterInspection();
        }

        private void ProblèmeToolStripMenuItemDeclarer_Click(object sender, EventArgs e)
        {
            MdiChild = new FormSaisieProbleme();
        }

        private void ProblèmesToolStripMenuItemConsulter_Click(object sender, EventArgs e)
        {
            MdiChild = new FormConsulterProbleme();
        }

        private void InspectionToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
