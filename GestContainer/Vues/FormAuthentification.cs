﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.DirectoryServices;

namespace GestContainer
{
    public partial class FormAuthentification : Form
    {
        private Form _mdiChild;

        private Form MdiChild
        {
            get { return _mdiChild; }
            set
            {
                if (_mdiChild != null)
                {
                    _mdiChild.Dispose();
                }
                _mdiChild = value;
                _mdiChild.MdiParent = this;
                _mdiChild.MaximumSize = _mdiChild.Size;
                _mdiChild.MinimumSize = _mdiChild.Size;
                _mdiChild.Show();
            }
        }

        public FormAuthentification()
        {
            InitializeComponent();

            // Permet de masquer les caractères en les remplaçant par * lors de la saisie du mot de passe
            TextBoxMDP.PasswordChar = '*';
        }

        private void FormAuthentification_Load(object sender, EventArgs e)
        {

        }

        private void GroupBox_Authentification_Enter(object sender, EventArgs e)
        {

        }

        private void TextBoxIdentifiant_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBoxMDP_TextChanged(object sender, EventArgs e)
        {

        }

        private void Button_Valider_Click(object sender, EventArgs e)
        {
            if (TextBoxIdentifiant.Text != "" && TextBoxMDP.Text != "")
            {
                try
                {
                    // connexion à l'annuaire LDAP
                    DirectoryEntry Ldap = new DirectoryEntry("LDAP://tho-hdesk.tholdi.com/OU=OU-Fret&Maintenance, OU=OU-Paris, DC=tholdi, DC=com", @"gae.bon", "Xazerty1");

                    // l'objet DirectorySearcher permet d'effectuer une recherche
                    DirectorySearcher searcher = new DirectorySearcher(Ldap);

                    // On modifie le filtre pour ne chercher que les utilisateurs
                    searcher.Filter = "(objectClass=user)";

                    //pour chaque résultat trouvé
                    foreach(SearchResult result2 in searcher.FindAll())
                    {
                        DirectoryEntry DirEntry2 = result2.GetDirectoryEntry();
                    }

                    //nouvel objet pour instancier la recherche
                    DirectorySearcher searcher2 = new DirectorySearcher(Ldap);

                    //On modifie le filtre pour ne chercher que l'user demandé
                    searcher2.Filter = "(saMAccountName=" + TextBoxIdentifiant.Text + ")";

                    // Pas de boucle foreach car on ne cherche qu'un user
                    SearchResult result = searcher2.FindOne();

                    // On récupère l'objet trouvé lors de la recherche
                    DirectoryEntry DirEntry = result.GetDirectoryEntry();

                    // On vérifie que son nom correspond à ce qu'il y a dans la saisie
                    if (string.Compare(DirEntry.Properties["sn"].Value.ToString(), TextBoxMDP.Text) == 0)
                    {
                        MessageBox.Show("Authentification réussie.");
                        if (DirEntry.Properties.Contains("physicalDeliveryOfficeName"))
                        {
                            MdiChild = new FormMenuChef();
                        }
                        else
                        {
                            MdiChild = new FormMenuDocker();
                        }                        
                    }
                    else
                    {
                        MessageBox.Show("Erreur d'authentification !");
                    }

                }
                catch (Exception Ex)
                {
                    MessageBox.Show("erreur LDAP " + Ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Veuillez remplir les champs de saisie.");
            }
        }
    }
}