﻿namespace GestContainer
{
    partial class FormMenuChef
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.InspectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.InspectionToolStripMenuItemSaisieInspection = new System.Windows.Forms.ToolStripMenuItem();
            this.InspectionsToolStripMenuItemConsulterInsp = new System.Windows.Forms.ToolStripMenuItem();
            this.ProblèmesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProblèmeToolStripMenuItemDeclarer = new System.Windows.Forms.ToolStripMenuItem();
            this.ProblèmesToolStripMenuItemConsulter = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuStrip1
            // 
            this.MenuStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MenuStrip1.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.InspectionToolStripMenuItem,
            this.ProblèmesToolStripMenuItem});
            this.MenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip1.Name = "MenuStrip1";
            this.MenuStrip1.Size = new System.Drawing.Size(353, 113);
            this.MenuStrip1.TabIndex = 1;
            this.MenuStrip1.Text = "MenuStrip1";
            // 
            // InspectionToolStripMenuItem
            // 
            this.InspectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.InspectionToolStripMenuItemSaisieInspection,
            this.InspectionsToolStripMenuItemConsulterInsp});
            this.InspectionToolStripMenuItem.Name = "InspectionToolStripMenuItem";
            this.InspectionToolStripMenuItem.Size = new System.Drawing.Size(163, 109);
            this.InspectionToolStripMenuItem.Text = "Inspection";
            this.InspectionToolStripMenuItem.Click += new System.EventHandler(this.InspectionToolStripMenuItem_Click);
            // 
            // InspectionToolStripMenuItemSaisieInspection
            // 
            this.InspectionToolStripMenuItemSaisieInspection.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.InspectionToolStripMenuItemSaisieInspection.Name = "InspectionToolStripMenuItemSaisieInspection";
            this.InspectionToolStripMenuItemSaisieInspection.Size = new System.Drawing.Size(298, 30);
            this.InspectionToolStripMenuItemSaisieInspection.Text = "Saisir une inspection";
            this.InspectionToolStripMenuItemSaisieInspection.Click += new System.EventHandler(this.InspectionToolStripMenuItemSaisieInspection_Click);
            // 
            // InspectionsToolStripMenuItemConsulterInsp
            // 
            this.InspectionsToolStripMenuItemConsulterInsp.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.InspectionsToolStripMenuItemConsulterInsp.Name = "InspectionsToolStripMenuItemConsulterInsp";
            this.InspectionsToolStripMenuItemConsulterInsp.Size = new System.Drawing.Size(298, 30);
            this.InspectionsToolStripMenuItemConsulterInsp.Text = "Consulter les inspections";
            this.InspectionsToolStripMenuItemConsulterInsp.Click += new System.EventHandler(this.InspectionsToolStripMenuItemConsulterInsp_Click);
            // 
            // ProblèmesToolStripMenuItem
            // 
            this.ProblèmesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProblèmeToolStripMenuItemDeclarer,
            this.ProblèmesToolStripMenuItemConsulter});
            this.ProblèmesToolStripMenuItem.Name = "ProblèmesToolStripMenuItem";
            this.ProblèmesToolStripMenuItem.Size = new System.Drawing.Size(166, 109);
            this.ProblèmesToolStripMenuItem.Text = "Problèmes";
            // 
            // ProblèmeToolStripMenuItemDeclarer
            // 
            this.ProblèmeToolStripMenuItemDeclarer.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.ProblèmeToolStripMenuItemDeclarer.Name = "ProblèmeToolStripMenuItemDeclarer";
            this.ProblèmeToolStripMenuItemDeclarer.Size = new System.Drawing.Size(292, 30);
            this.ProblèmeToolStripMenuItemDeclarer.Text = "Déclarer un problème";
            this.ProblèmeToolStripMenuItemDeclarer.Click += new System.EventHandler(this.ProblèmeToolStripMenuItemDeclarer_Click);
            // 
            // ProblèmesToolStripMenuItemConsulter
            // 
            this.ProblèmesToolStripMenuItemConsulter.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.ProblèmesToolStripMenuItemConsulter.Name = "ProblèmesToolStripMenuItemConsulter";
            this.ProblèmesToolStripMenuItemConsulter.Size = new System.Drawing.Size(292, 30);
            this.ProblèmesToolStripMenuItemConsulter.Text = "Consulter les problèmes";
            this.ProblèmesToolStripMenuItemConsulter.Click += new System.EventHandler(this.ProblèmesToolStripMenuItemConsulter_Click);
            // 
            // FormMenuChef
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 113);
            this.Controls.Add(this.MenuStrip1);
            this.IsMdiContainer = true;
            this.Name = "FormMenuChef";
            this.Text = "FormMenuChef";
            this.Load += new System.EventHandler(this.FormMenuChef_Load);
            this.MenuStrip1.ResumeLayout(false);
            this.MenuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.MenuStrip MenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem InspectionToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem InspectionToolStripMenuItemSaisieInspection;
        internal System.Windows.Forms.ToolStripMenuItem InspectionsToolStripMenuItemConsulterInsp;
        internal System.Windows.Forms.ToolStripMenuItem ProblèmesToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ProblèmeToolStripMenuItemDeclarer;
        internal System.Windows.Forms.ToolStripMenuItem ProblèmesToolStripMenuItemConsulter;
    }
}