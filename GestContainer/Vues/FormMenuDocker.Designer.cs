﻿namespace GestContainer
{
    partial class FormMenuDocker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.InspectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaisirInspectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ConsulterInspectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeclarerProblemeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuStrip1
            // 
            this.MenuStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.InspectionToolStripMenuItem,
            this.DeclarerProblemeToolStripMenuItem});
            this.MenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip1.Name = "MenuStrip1";
            this.MenuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.MenuStrip1.Size = new System.Drawing.Size(491, 127);
            this.MenuStrip1.TabIndex = 1;
            this.MenuStrip1.Text = "MenuStrip1";
            // 
            // InspectionToolStripMenuItem
            // 
            this.InspectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaisirInspectionToolStripMenuItem,
            this.ConsulterInspectionsToolStripMenuItem});
            this.InspectionToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold);
            this.InspectionToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.InspectionToolStripMenuItem.Name = "InspectionToolStripMenuItem";
            this.InspectionToolStripMenuItem.Size = new System.Drawing.Size(163, 123);
            this.InspectionToolStripMenuItem.Text = "Inspection";
            // 
            // SaisirInspectionToolStripMenuItem
            // 
            this.SaisirInspectionToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.SaisirInspectionToolStripMenuItem.Name = "SaisirInspectionToolStripMenuItem";
            this.SaisirInspectionToolStripMenuItem.Size = new System.Drawing.Size(270, 30);
            this.SaisirInspectionToolStripMenuItem.Text = "Saisir Inspection";
            this.SaisirInspectionToolStripMenuItem.Click += new System.EventHandler(this.SaisirInspectionToolStripMenuItem_Click);
            // 
            // ConsulterInspectionsToolStripMenuItem
            // 
            this.ConsulterInspectionsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.ConsulterInspectionsToolStripMenuItem.Name = "ConsulterInspectionsToolStripMenuItem";
            this.ConsulterInspectionsToolStripMenuItem.Size = new System.Drawing.Size(270, 30);
            this.ConsulterInspectionsToolStripMenuItem.Text = "Consulter Inspections";
            this.ConsulterInspectionsToolStripMenuItem.Click += new System.EventHandler(this.ConsulterInspectionsToolStripMenuItem_Click);
            // 
            // DeclarerProblemeToolStripMenuItem
            // 
            this.DeclarerProblemeToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold);
            this.DeclarerProblemeToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.DeclarerProblemeToolStripMenuItem.Name = "DeclarerProblemeToolStripMenuItem";
            this.DeclarerProblemeToolStripMenuItem.Size = new System.Drawing.Size(308, 123);
            this.DeclarerProblemeToolStripMenuItem.Text = "Déclarer un problème";
            this.DeclarerProblemeToolStripMenuItem.Click += new System.EventHandler(this.DeclarerProblemeToolStripMenuItem_Click);
            // 
            // FormMenuDocker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 127);
            this.Controls.Add(this.MenuStrip1);
            this.IsMdiContainer = true;
            this.Name = "FormMenuDocker";
            this.Text = "FormMenuDocker";
            this.Load += new System.EventHandler(this.FormMenuDocker_Load);
            this.MenuStrip1.ResumeLayout(false);
            this.MenuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.MenuStrip MenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem InspectionToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem SaisirInspectionToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ConsulterInspectionsToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem DeclarerProblemeToolStripMenuItem;
    }
}