﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace GestContainer
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Initialisation de la connexion
            MySqlConnection connexionMySql = new MySqlConnection();
            connexionMySql.ConnectionString = "Database=GestContainer;Data Source=localhost;User Id=root;Password=root";
            DataBaseAccess.Connexion = connexionMySql;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormAuthentification());
        }
    }
}
